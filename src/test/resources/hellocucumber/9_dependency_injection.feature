# Sometimes, we need to share state between steps that live in different step definition classes.
# We can share state with dependency injection. Cucumber supports several frameworks.

# TODO share the state of the weekend feature but now with dependency injection, so that other step definitions can use the state.

Feature: Is it weekend yet?
  Everybody wants to know when it's weekend

  Scenario: Monday isn't weekend
    Given today is Monday
    When I ask whether it's weekend yet
    Then I should be told "Nope"

  ## Implement this Scenario
  Scenario: Saturday is weekend
    Given today is Saturday
    When I ask whether it's weekend yet
    Then I should be told "Yes, it is."
