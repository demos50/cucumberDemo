# Now translate a test to dutch. Default language is en
# language: en
Feature: Is it weekend yet?
  Everybody wants to know when it's weekend

  Scenario: Monday isn't weekend
    Given today is Monday
    When I ask whether it's weekend yet
    Then I should be told "Nope"

# English Keyword	Dutch equivalent(s)
#  feature	        Functionaliteit
#  background	    Achtergrond
#  scenario	        Voorbeeld / Scenario
#  scenarioOutline	Abstract Scenario
#  examples	        Voorbeelden
#  given	            Gegeven /Stel
#  when	            Als / Wanneer
#  then	            Dan
#  and	            En
#  but	            Maar
#  rule	            Rule