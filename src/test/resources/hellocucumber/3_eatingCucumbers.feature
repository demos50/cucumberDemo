Feature: We like eating cucumbers

  Scenario: eat 5 out of 12
    Given there are 12 cucumbers
    When I eat 5 cucumbers
    Then I should have 7 cucumbers

  Scenario: eat 5 out of 20
    Given there are 20 cucumbers
    When I eat 5 cucumbers
    Then I should have 15 cucumbers

  Scenario: eat 1 out of 20
    Given there are 20 cucumbers
    When I eat 1 cucumber
    Then I should have 19 cucumbers


 # How can these tests be simplified? They use a lot of the same steps.
 # Implement these tests in a more efficient way. Hint: use Scenario Outline and examples
