# Organize your work with tags: @weekend @weekday @allWeek @Doctor etc
# Now run all the weekend tests with 'mvn test -Dcucumber.options="--tags '@debug1 and @debug2'"' 
# Now run all tests except the doctor tests 'mvn clean test -Dcucumber.filter.tags="not @MyTag"'
@Example
Feature: An example

  Scenario: The example
    Given an example scenario
    When all step definitions are implemented
    Then the scenario passes
