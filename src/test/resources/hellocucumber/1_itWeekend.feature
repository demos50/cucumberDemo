Feature: Is it weekend yet?
  Everybody wants to know when it's weekend

  Scenario: Monday isn't weekend
    Given today is Monday
    When I ask whether it's weekend yet
    Then I should be told "Nope"

  ## Implement this Scenario
  Scenario: Saturday is weekend
    Given today is Saturday
    When I ask whether it's weekend yet
    Then I should be told "Yes, it is."
