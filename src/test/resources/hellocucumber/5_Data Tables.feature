# use a data table to do stuff
Feature: Is it during the week again?

  Scenario: Datatables
    Given these Users:
      | name            | date of  birth  |
      | Michael Jackson | August 29, 1958 |
      | Elvis           | January 8, 1935 |
      | John Lennon     | October 9, 1940 |
    Then their age is:
    # TODO Fill in the rest