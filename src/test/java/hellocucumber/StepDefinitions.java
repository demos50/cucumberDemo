package hellocucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StepDefinitions {
    private final AppService appService;
    private String today;
    private String actualAnswer;

    public StepDefinitions(AppService appService) {
        this.appService = Objects.requireNonNull(appService, "appService must not be null");
    }

    @Given("an example scenario")
    public void anExampleScenario() {
        //setup situation
    }

    @When("all step definitions are implemented")
    public void allStepDefinitionsAreImplemented() {
        //perform an action
    }

    @Then("the scenario passes")
    public void theScenarioPasses() {
        //do assertions here
    }

    @Given("today is Monday")
    public void today_is_Monday() {
        today = "Monday";
    }

    @When("I ask whether it's weekend yet")
    public void i_ask_whether_it_s_Weekend_yet() {
        actualAnswer = IsItWeekend.isItWeekend(today);
    }

    @Then("I should be told {string}")
    public void i_should_be_told(String expectedAnswer) {
        assertEquals(expectedAnswer, actualAnswer);
    }

    @Given("these Users:")
    public void these_users(io.cucumber.datatable.DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        var map = dataTable.entries();
        System.out.println(map);
        throw new io.cucumber.java.PendingException();
    }

    class IsItWeekend {
        static String isItWeekend(String today) {
            return "Nope";
        }
    }

}
